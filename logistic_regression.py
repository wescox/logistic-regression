import numpy as np
import pandas as pd
from math import exp, log
from random import randint

import matplotlib.pyplot as plt

# Import the data
train_data = pd.read_csv('usedCar_trainingData.csv', sep=',',header=0)
num_data = train_data.shape[0]

train_data['bias'] = pd.Series(np.ones(num_data))

features = ['bias','mileage','num_services']

# Initialize the parameters to all ones
parameters = np.ones(len(features))

# Hyperparameter: leaning rate
alpha = 0.05

def scale_features():
	global train_data

	# Only scale the columns our algorithm will be learning from
	cols = ['mileage','num_services']

	# Record what the min and max values were, so the data can be restored
	minimums = train_data[cols].min()
	maximums = train_data[cols].max()

	# Perform min-max scaling
	normalized_train_data = (train_data[cols]-train_data[cols].min())/(train_data[cols].max()-train_data[cols].min())
	train_data[cols] = normalized_train_data
	
	return minimums, maximums


def hypothesis(theta):
	T_x = theta_x(theta)
	return 1 / (1 + np.exp(-T_x))

def theta_x(theta):
	global train_data, features
	return np.dot(train_data.loc[:,features], theta)

def update_parameters():
	global parameters, train_data, num_data, features

	# Select a random row from the training data (stochastic gradient descent)
	random_x = train_data.iloc[randint(0,num_data-1)]
	
	# Update the parameters
	targetDifference = alpha * (random_x['lemon'] - random_x['h'])
	parameters += targetDifference * random_x.loc[features].values

def plot_learning_convergence(plot_data):

	# Identify which datapoints are lemons
	lemons = train_data[train_data['lemon'] == 1]
	not_lemons = train_data[train_data['lemon'] == 0]

	plt.subplot(2,1,1)
	# Scale the data back to its real values before plotting
	plt.plot(lemons['mileage']*(maximums['mileage']-minimums['mileage']) + minimums['mileage'], 
		lemons['num_services']*(maximums['num_services']-minimums['num_services']) + minimums['num_services'], 'ro')
	plt.plot(not_lemons['mileage']*(maximums['mileage']-minimums['mileage']) + minimums['mileage'], 
		not_lemons['num_services']*(maximums['num_services']-minimums['num_services']) + minimums['num_services'], 'bo')

	plt.xlabel('Mileage')
	plt.ylabel('Number of Services')

	# Calculate line of best fit
	x2l = (log(1) - parameters[0])/parameters[2]
	x2r = (log(1) - parameters[0] - parameters[1])/parameters[2]

	plt.plot(np.array([0,1])*(maximums['mileage']-minimums['mileage']) + minimums['mileage'], 
		np.array([x2l, x2r])*(maximums['num_services']-minimums['num_services']) + minimums['num_services'],'g-')

	plt.legend(['Lemon', 'Not Lemon', 'Lemon Boundary'])

	plt.subplot(2,1,2)
	# Plot the target
	plt.plot(plot_data['iteration'],plot_data['goal_data'], 'g-')

	# Plot how well our model matched the target
	plt.plot(plot_data['iteration'],plot_data['learn_data'], 'r-')

	plt.xlabel('Iterations')
	plt.ylabel('Number of Correct Predictions')
	plt.legend(['Goal', 'Logistic Regression'])
	plt.show()

# Scale the data between 0 and 1, and remember the previous min and max values
minimums,maximums = scale_features()


# We want to observe if our hypothesis is getting closer as the code iterates
data_to_plot = {'learn_data' : [], 'iteration' : [], 'goal_data': []}

# Iterate the algorithm, to allow stochastic gradient descent to do its thing
for loop in range(0,1000):
	if loop % 100 == 0:
		print("Loop " + str(loop))

	# Calculate the hypothesis for the current parameters
	hyp = hypothesis(parameters)

	# Store the current hypothesis, and convert it to a binary good/bad
	train_data['h'] = hyp
	train_data['hypothesis'] = np.where(hyp>=0.5, 1, 0)

	# Count how many predictions we got right
	good_predictions = train_data[train_data['lemon'] == train_data['hypothesis']]['lemon'].count()

	# Apply gradient descent
	update_parameters()

	data_to_plot['iteration'].append(loop)
	data_to_plot['learn_data'].append(good_predictions)
	# The goal is that we get every prediction right, so the goal data is the number of training points
	data_to_plot['goal_data'].append(num_data)

# Output how well we did and plot the convergence
print("Learning accuracy " + str(data_to_plot['learn_data'][-1] / data_to_plot['goal_data'][-1]))
plot_learning_convergence(pd.DataFrame(data_to_plot))
